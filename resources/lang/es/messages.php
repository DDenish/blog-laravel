<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Spanish Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'laravel' => 'Laravel',
    'blog' => 'Blog',
    'login'=>'Iniciar sesión',
    'register'=> 'Registro',
    'search'=> 'Buscar',
    'email_address'=> 'Dirección de correo electrónico',
    'password'=> 'Contraseña',
    'remember-me'=> 'Recuérdame',
    'forgot-password'=> '¿Olvidaste tu contraseña?',
    'reset-password'=> 'Restablecer la contraseña',
    'reset-button'=> 'Enviar contraseña Restablecer enlace',
    'posts'=> 'Publicaciones',
    'categories'=> 'Categorías',
    'edit'=> 'Editar',
    'delete'=> 'Borrar',
    'creat_button_post'=> 'Nueva publicación',
    'creat_button_categoy'=> 'Nueva categoría',
    'label_title'=> 'Título',
    'label_slug'=> 'Babosa',
    'label_content'=> 'Contenido',
    'label_summary'=> 'Resumen',
    'label_category'=> 'Categoría',
    'label_image'=> 'Imagen',
    'remove_link'=> 'retirar',
    'new'=> 'Nuevo',
    'post'=> 'Enviar',
    'category'=> 'Categoría',
    'label_name'=> 'Nombre',
    'label_submit_button'=> 'Enviar',
    'label_back_button'=>'Espalda',
    'no'=>'No',
    'action'=>'Acción',
    'name'=>"Nombre"
];
