<?php

return [

    /*
    |--------------------------------------------------------------------------
    | English Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'laravel' => 'Laravel',
    'blog' => 'Blog',
    'login'=>'Login',
    'register'=> 'Register',
    'search'=> 'Search',
    'email_address'=> 'E-Mail Address',
    'password'=> 'Password',
    'remember-me'=> 'Remember me',
    'forgot-password'=> 'Forgot Your Password?',
    'reset-password'=> 'Reset Password',
    'reset-button'=> 'Send Password Reset Link',
    'posts'=> 'Posts',
    'categories'=> 'Categories',
    'edit'=> 'Edit',
    'delete'=> 'Delete',
    'creat_button_post'=> 'New Post',
    'creat_button_categoy'=> 'New Category',
    'label_title'=> 'Title',
    'label_slug'=> 'Slug',
    'label_content'=> 'Post Content',
    'label_summary'=> 'Post Summary',
    'label_category'=> 'Category',
    'label_image'=> 'Image',
    'remove_link'=> 'Remove',
    'new'=> 'New',
    'post'=> 'Post',
    'category'=> 'Category',
    'label_name'=> 'Name',
    'label_submit_button'=> 'Submit',
    'label_back_button'=>'Back',
    'no'=>'No',
    'action'=>'Action',
    'name'=>"Name"
];
