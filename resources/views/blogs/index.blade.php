@extends('layouts.app')
@section('css')
    <style>
        a, a:hover {
            color: white;
        }
    </style>
@endsection
@section('content')
<div class="container">
        {!! Form::open(['method'=>'get']) !!}
        <div class="row">
            <div class="col-sm-5 form-group">
                <div class="input-group">
                    <input class="form-control" id="search"
                           value="{{ request('search') }}"
                           placeholder="Search" name="search"
                           type="text" id="search"/>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-warning"
                        >
                            Search
                        </button>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
        <div>
            <div>
            <ul class="post-list">
                 @if(!$posts->isEmpty())
                     @foreach($posts as $post)
                     <li>
                        <h3><b>{{ $post->title }}</b></h3>
                        <h4>{{ $post->cat_name }}</h4>
                        <p class="excerpt">{{ $post->content }}</p>
                        <p class="excerpt">{{ $post->summary }}</p>
                    </li>
                    @endforeach
                    {{$posts->links()}}
                @else
                    <p>No records Found</p>
                @endif
            </ul>
            </div>
        </div>
    </div>
@endsection
