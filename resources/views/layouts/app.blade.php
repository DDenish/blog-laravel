<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                    @guest
                     <a class="navbar-brand" href="{{ url('/blog') }}">
                        @lang('messages.blog')
                    </a>
                    @else
                    <a class="navbar-brand" href="{{ url('/blogposts') }}">
                        @lang('messages.posts')
                    </a>
                    <a class="navbar-brand" href="{{ url('/blogposts/categories') }}">
                        @lang('messages.categories')
                    </a>
                    <a class="navbar-brand" href="{{ url('/languages') }}">
                        Languages
                    </a>
                    @endguest
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        
                        @guest
                            <li><a href="{{ route('login') }}">@lang('messages.login')</a></li>
                            <li><a href="{{ route('register') }}">@lang('messages.register')</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
                
                <select id="language-drop" name="language">
                    @foreach(App\Languages::all() as $lang)
                        @if( Config::get('app.locale') == $lang->code)    
                        <option value='{{ $lang->code }}' selected="selected">{{ $lang->name }}</option>
                        @else 
                        <option value='{{ $lang->code }}'>{{ $lang->name }}</option>
                        @endif                        
                    @endforeach
                </select>
            </div>
        </nav>
        @yield('content')
    </div>
    <!-- Scripts -->
    <script>
        $(document).ready(function(){
            $('#language-drop').change(function(){
                var url = window.location.href; 
                var newurl=url.replace('{{LaravelLocalization::getCurrentLocale()}}',$(this).val());
                window.location.href=newurl;
            });
        });
    </script>
    <script src="{{ asset('public/js/app.js') }}"></script>
</body>
</html>