@extends('layouts.app')
@section('css')
    <style>
        a, a:hover {
            color: white;
        }
    </style>
@endsection
@section('content')
<div class="container">
        <div class="float-right">
            <a href="{{url('blogposts/create')}}" class="btn btn-primary">{{ __('messages.new') }}</a>
        </div>
        <hr/>
        {!! Form::open(['method'=>'get']) !!}
        <div class="row">
            <div class="col-sm-5 form-group">
                <div class="input-group">
                    <input class="form-control" id="search"
                           value="{{ request('search') }}"
                           placeholder="{{ __('messages.search') }}" name="search"
                           type="text" id="search"/>
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-warning"
                        >
                            {{ __('messages.search') }}
                        </button>
                    </div>
                </div>
            </div>
            <input type="hidden" value="{{request('field')}}" name="field"/>
            <input type="hidden" value="{{request('sort')}}" name="sort"/>
        </div>
        {!! Form::close() !!}
        <table class="table table-bordered bg-light">
            <thead class="bg-dark" style="color: white">
            <tr>
                <th width="60px" style="vertical-align: middle;text-align: center">{{ __('messages.no') }}</th>
                <th style="vertical-align: middle">
                    <a href="{{url('blogposts')}}?search={{request('search')}}&field=title&sort={{request('sort','asc')=='asc'?'desc':'asc'}}">
                        {{ __('messages.name') }}
                    </a>
                    {{request('field','title')=='title'?(request('sort','asc')=='asc'?'&#9652;':'&#9662;'):''}}
                </th>
                
                <th width="130px" style="vertical-align: middle">{{ __('messages.action') }}</th>
            </tr>
            </thead>
            <tbody>
            @php
                $i=1;
            @endphp
            @foreach($posts as $post)
                <tr>
                    <th style="vertical-align: middle;text-align: center">{{$i++}}</th>
                    <td style="vertical-align: middle">{{ $post->title }}</td>
                    <td style="vertical-align: middle" align="center">
                        <form id="frm_{{$post->post_id}}"
                              action="{{url(LaravelLocalization::getCurrentLocale().'/blogposts/delete/'.$post->post_id)}}"
                              method="post" style="padding-bottom: 0px;margin-bottom: 0px">
                            <a class="btn btn-primary btn-sm" title="Edit"
                               href="{{url('blogposts/update/'.$post->post_id)}}">
                                {{ __('messages.edit') }}</a>
                            
                            {{csrf_field()}}
                            <a class="btn btn-danger btn-sm" title="Delete"
                               href="javascript:if(confirm('Are you sure want to delete?')) $('#frm_{{$post->post_id}}').submit()">
                                {{ __('messages.delete') }}
                            </a>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <nav>
            <ul class="pagination justify-content-end">
                {{$posts->links()}}
            </ul>
        </nav>
    </div>
@endsection
