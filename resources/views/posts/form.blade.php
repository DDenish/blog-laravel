@extends('layouts.app')
@section('css')
    <style>
        .form-group.required label:after {
            content: " *";
            color: red;
            font-weight: bold;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">

            <h1>{{isset($post_lang)?'Edit':'New'}} @lang('messages.post')</h1>
            <hr/>
            @if(isset($post_lang))
                {!! Form::model($post_lang,['method'=>'put','files'=>true ]) !!}
                <input type="hidden" id="lang_id" name="lang_id" value="{{$post_lang->lang_id}}" /> 
                <input type="hidden" id="post_id" name="post_id" value="{{$post_lang->post_id}}" /> 
            @else
                {!! Form::open(['files'=>true]) !!}
            @endif
            <div class="form-group row required">
                <label for="title" class="col-form-label col-md-3 col-lg-2">@lang('messages.label_title')</label>
                <div class="col-md-8">
                    {!! Form::text("title",null,["class"=>"form-control".($errors->has('title')?" is-invalid":""),"autofocus",'placeholder'=>'title']) !!}
                    {!! $errors->first('title','<span class="invalid-feedback">:message</span>') !!}
                </div>
            </div>
            <div class="form-group row required">
                <label for="sluf" class="col-form-label col-md-3 col-lg-2">@lang('messages.label_slug')</label>
                <div class="col-md-8">
                    {!! Form::text("slug",null,["class"=>"form-control".($errors->has('slug')?" is-invalid":""),"autofocus",'placeholder'=>'slug']) !!}
                    {!! $errors->first('slug','<span class="invalid-feedback">:message</span>') !!}
                </div>
            </div>
            <div class="form-group row required">
                <label for="summary" class="col-form-label col-md-3 col-lg-2">@lang('messages.label_summary')</label>
                <div class="col-md-8">
                    {!! Form::textarea("summary",null,["class"=>"form-control".($errors->has('summary')?" is-invalid":""),"autofocus",'placeholder'=>'summary']) !!}
                    {!! $errors->first('summary','<span class="invalid-feedback">:message</span>') !!}
                </div>
            </div>
            <div class="form-group row required">
                <label for="content" class="col-form-label col-md-3 col-lg-2">@lang('messages.label_content')</label>
                <div class="col-md-8">
                    {!! Form::textarea("content",null,["class"=>"form-control".($errors->has('content')?" is-invalid":""),"autofocus",'placeholder'=>'content']) !!}
                    {!! $errors->first('content','<span class="invalid-feedback">:message</span>') !!}
                </div>
            </div>
            <div class="form-group row">
                <label for="category" class="col-form-label col-md-3 col-lg-2">@lang('messages.label_category')</label>
                <div class="col-md-8">
                    @if(isset($post))
                        @foreach($categories as $key => $category)
                        <label class="form-checkbox form-normal form-green form-text">
                            <input type="checkbox" name="category_id[]" id="{{ $key }}" value="{{ $key }}" @if(in_array($key, $selected_categories)) checked @endif > <span>{{ $category }}</span><br/>
                        </label>
                        @endforeach
                    @else
                       @foreach($categories as $key => $category)
                        <label class="form-checkbox form-normal form-green form-text">
                            <input type="checkbox" name="category_id[]" id="{{ $key }}" value="{{ $key }}"> <span>{{ $category }}</span><br/>
                        </label>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="form-group row" id="images">
                <label for="image" class="col-form-label col-md-3 col-lg-2">@lang('messages.label_image')</label>
                <div class="col-md-5">
                    @if(isset($post_lang))
                        @if(strpos($post_lang->image,',') !== false) 
                           @php
                                $i=1;
                           @endphp
                          {!! Form::file("image[]",["id"=>"image","class"=>"form-control","multiple"=>"multiple"]) !!}
                           @foreach(explode(',', $post_lang->image) as $index => $image) 
                                @if($image != "")
                                <div>
                                    <img id="preview"
                                     src="{{asset((isset($image) && $image!='')?'uploads/'.$image:'public/images/noimage.jpg')}}"
                                     height="200px" width="200px" />
                                    <a style="color: red" id="delete_image">Remove</a>
                                    <input type="hidden" id="image_name" value="{{$image}}" /> 
                                </div>
                                @endif
                            @endforeach 
                        @else
                            @if($post_lang->image != "")
                                {!! Form::file("image[]",["id"=>"image","class"=>"form-control","multiple"=>"multiple"]) !!}
                                <img id="preview"
                                 src="{{asset('uploads/'.$post_lang->image)}}"
                                 height="200px" width="200px" />
                                <a style="color: red" id="delete_image">Remove</a>
                                <input type="hidden" id="image_name" value="{{$post_lang->image}}" /> 
                            @else
                                {!! Form::file("image[]",["id"=>"image","class"=>"form-control","multiple"=>"multiple"]) !!}
                            @endif                
                        @endif
                    @else
                        {!! Form::file("image[]",["id"=>"image","class"=>"form-control","multiple"=>"multiple"]) !!}    
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-3 col-lg-2"></div>
                <div class="col-md-4">
                    <a href="{{url('blogposts')}}" class="btn btn-danger">
                        @lang('messages.label_back_button')</a>
                    {!! Form::button("Save",["type" => "submit","class"=>"btn
                btn-primary"])!!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            $('#delete_image').click(function(e){
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "{{ url(LaravelLocalization::getCurrentLocale().'/blogposts/ajaxrequest') }}",
                    data  :  {
                        'post_id': $('#post_id').val(),
                        'image_name': $('#image_name').val(),
                        'lang_id':$('#lang_id').val()
                    },
                    success:function(result)
                    {
                        alert(result.success);
                        console.log(result.success);
                    },
                    error: function(jqXHR, textStatus, errorThrown) { 
                        // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            });
        });
    </script>
    <script type="text/javascript">
        function changeProfile() {
            $('#image').click();
        }
        $('#image').change(function () {
            var imgPath = $(this)[0].value;
            var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
                readURL(this);
            else
                alert("Please select image file (jpg, jpeg, png).")
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.readAsDataURL(input.files[0]);
                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                    $('#remove').val(0);
                }
            }
        }
        
        function removeImage() {
            $('#preview').attr('src', '{{url('public/images/noimage.jpg')}}');
            $('#remove').val(1);
        }

        function editchangeProfile(imageid) {
            $('#'+imageid).click();
        }
        
        function editremoveImage(previewid,removeid) {
            $('#'+previewid).attr('src', '{{url('public/images/noimage.jpg')}}');
            $('#'+removeid).val(1);
        }
    </script>
@endsection
