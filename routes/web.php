<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => LaravelLocalization::setLocale(),'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]],function(){

	Route::get('/', 'BlogController@index');

	Auth::routes();

	Route::get('/home', 'HomeController@index')->name('home');

	Route::group(['prefix' => '/blogposts/'], function () {
	    Route::get('/', 'PostsController@index');
	    Route::match(['get', 'post'], 'create', 'PostsController@create');
	    Route::match(['get', 'put','post'], 'update/{id}', 'PostsController@update');
	    Route::post('delete/{id}', 'PostsController@delete');
	    Route::post('ajaxrequest','PostsController@ajaxrequest');
	});

	Route::group(['prefix' => 'blogposts/categories'], function () {
	    Route::get('/', 'CategoriesController@index');
	    Route::match(['get', 'post'], 'create', 'CategoriesController@create');
	    Route::match(['get', 'put'], 'update/{id}', 'CategoriesController@update');
	    Route::delete('delete/{id}', 'CategoriesController@delete');

	});

	Route::group(['prefix' => 'languages/'], function () {
	    Route::get('/', 'LanguagesController@index');
	    Route::match(['get', 'post'], 'create', 'LanguagesController@create');
	    Route::match(['get', 'put'], 'update/{id}', 'LanguagesController@update');
	    Route::delete('delete/{id}', 'LanguagesController@delete');

	});

	Route::get('/blog', 'BlogController@index');


	/* Language Traslation */
	/*Route::group(['prefix' => '{lang?}/blogposts/'], function ($lang=null) {
	    App::setLocale(Request::segment(1));
	    Route::get('/', 'PostsController@index');
	    Route::match(['get', 'post'], 'create', 'PostsController@create');
	    Route::match(['get', 'put'], 'update/{id}', 'PostsController@update');
	    Route::delete('delete/{id}', 'PostsController@delete');
	    Route::post('ajaxrequest','PostsController@ajaxrequest');
	});

	Route::group(['prefix' => '{lang?}/blogposts/categories'], function ($lang=null) {
	    App::setLocale(Request::segment(1));
	    Route::get('/', 'CategoriesController@index');
	    Route::match(['get', 'post'], 'create', 'CategoriesController@create');
	    Route::match(['get', 'put'], 'update/{id}', 'CategoriesController@update');
	    Route::delete('delete/{id}', 'CategoriesController@delete');
	});*/

});