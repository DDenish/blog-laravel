<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostlanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postlanguages', function (Blueprint $table) {
            $table->integer('post_id')->unsigned();
            $table->string('title')->nullable();
            $table->string('slug')->index();
            $table->text('summary')->nullable();
            $table->longText('content')->nullable();
            $table->primary(['post_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postlanguages');
    }
}
