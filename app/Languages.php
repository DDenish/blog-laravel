<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Languages extends Model
{
    public function posts_languages()
    {
       return $this->belongsToMany(Post::class,'posts_categories');
    }

    public function category_languages()
    {
       return $this->belongsToMany(Category::class,'posts_categories');
    }
}
