<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postlanguage extends Model
{
    public function categories()
    {
       return $this->belongsToMany(Category::class,'posts_categories');
    }
}
