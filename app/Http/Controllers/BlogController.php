<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Lang;
use App\Languages;
class BlogController extends Controller
{
    public function index(Request $request)
    {
    	$this->current_local=\Lang::getLocale();
        $this->getLang=Languages::where('code',$this->current_local)->first();
        $search = $request->get('search');
       	if(!empty($search))
       	{
            $posts = DB::table('posts as p')
                    ->leftJoin('postlanguages as pl', 'p.id', '=', 'pl.post_id')
                    ->leftJoin('posts_categories as c', 'p.id', '=', 'c.post_id')
                    ->leftJoin('categories as bc', 'c.category_id', '=', 'bc.id')
                    ->where('pl.lang_id',$this->getLang->id)
                    ->where('pl.title', 'LIKE', '%' . $search . '%')
                    ->orWhere('bc.name', 'LIKE', '%' . $search . '%')
                    ->groupBy('pl.id')
                    ->select('pl.*',DB::raw("group_concat(c.category_id) as cat_ids"),DB::raw("group_concat(bc.name) as cat_name"))
                    ->paginate(1);
            if(!empty($posts))
            {
                $posts->appends(['search' => $search]);
                   $posts=$posts;
            }
            else
            {
                   $posts=array();
            }
       
        }
        else
        {
        	$posts = DB::table('posts as p')
                    ->leftJoin('postlanguages as pl', 'p.id', '=', 'pl.post_id')
                    ->leftJoin('posts_categories as c', 'p.id', '=', 'c.post_id')
                    ->leftJoin('categories as bc', 'c.category_id', '=', 'bc.id')
                    ->where('pl.lang_id',$this->getLang->id)
                    ->groupBy('pl.id')
                    ->select('pl.*',DB::raw("group_concat(c.category_id) as cat_ids"),DB::raw("group_concat(bc.name) as cat_name"))
                    ->paginate(1);
            //echo "<pre>";print_r($posts);exit;
            if(!empty($posts))
            {
                   $posts=$posts;
            }
            else
            {
                   $posts=array();
            }
            //echo "<pre>";print_r($posts);exit;
        }
        return view('blogs.index', compact('posts'));
    }
}
