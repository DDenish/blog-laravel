<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Category;
class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
    	$search = $request->get('search');
        $field = $request->get('field') != '' ? $request->get('field') : 'name';
        $sort = $request->get('sort') != '' ? $request->get('sort') : 'asc';
        $categories = new Category();
        $categories = $categories->where('name', 'like', '%' . $search . '%')
            ->orderBy($field, $sort)
            ->paginate(5)
            ->withPath('?search=' . $search.'&field=' . $field . '&sort=' . $sort);
        //echo "<pre>";print_r($posts);exit;
        return view('categories.index', compact('categories'));
    }

    public function create(Request $request)
    {
        if ($request->isMethod('get'))
            return view('categories.form');
        else {
            $rules = [
                'name' => 'required',
                'slug' => 'required|alpha_dash|max:200|unique:categories,slug',
            ];
            $this->validate($request, $rules);
            $category = new Category();
            $category->name = $request->name;
            $category->slug =  $request->slug;
            $category->save();
            return redirect('/blogposts/categories');
        }
    }


    public function update(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('categories.form', ['category' => Category::find($id)]);
        else {
            $rules = [
                'name' => 'required',
                'slug' => 'required|alpha_dash|max:200|unique:categories,slug,'.$id,
            ];
            $this->validate($request, $rules);
            $category = Category::find($id);
            $category->name = $request->name;
            $category->slug =  $request->slug;
            $category->save();
            return redirect('/blogposts/categories');
        }
    }

    public function delete($id)
    {
        Category::destroy($id);
        return redirect('/blogposts/categories');
    }
}
