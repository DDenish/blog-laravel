<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Languages;
class LanguagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
    	$search = $request->get('search');
        $field = $request->get('field') != '' ? $request->get('field') : 'name';
        $sort = $request->get('sort') != '' ? $request->get('sort') : 'asc';
        $languages = new Languages();
        $languages = $languages->where('name', 'like', '%' . $search . '%')
            ->orderBy($field, $sort)
            ->paginate(5)
            ->withPath('?search=' . $search.'&field=' . $field . '&sort=' . $sort);
        //echo "<pre>";print_r($posts);exit;
        return view('languages.index', compact('languages'));
    }

    public function create(Request $request)
    {
        if ($request->isMethod('get'))
            return view('languages.form');
        else {
            $rules = [
                'name' => 'required',
                'code' => 'required|alpha_dash|max:200|unique:languages,code',
            ];
            $this->validate($request, $rules);
            $language = new languages();
            $language->name = $request->name;
            $language->code =  $request->code;
            $language->save();
            return redirect('/languages');
        }
    }


    public function update(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('languages.form', ['language' => languages::find($id)]);
        else {
            $rules = [
                'name' => 'required',
                'code' => 'required|alpha_dash|max:200|unique:languages,code,'.$id,
            ];
            $this->validate($request, $rules);
            $language = languages::find($id);
            $language->name = $request->name;
            $language->code =  $request->code;
            $language->save();
            return redirect('/languages');
        }
    }

    public function delete($id)
    {
        languages::destroy($id);
        return redirect('/languages');
    }
}
