<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\Languages;
use App\Postlanguage;
use DB;
use Illuminate\Support\Facades\File;
use Lang;
class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->current_local=\Lang::getLocale();
        $this->getLang=Languages::where('code',$this->current_local)->first();
        //echo \Lang::getLocale();exit;
        
    }

    public function index(Request $request)
    {
    	$search = $request->get('search');
        $field = $request->get('field') != '' ? $request->get('field') : 'title';
        $sort = $request->get('sort') != '' ? $request->get('sort') : 'asc';
        
        $posts = new Postlanguage();
        $posts = DB::table('posts as p')
            ->leftJoin('postlanguages as pl','p.id','=','pl.post_id')
            ->where('pl.title', 'like', '%' . $search . '%')
            ->where('pl.lang_id',$this->getLang->id)
            ->orderBy($field, $sort)
            ->select('pl.*')
            ->paginate(5)
            ->withPath('?search=' . $search.'&field=' . $field . '&sort=' . $sort);
        return view('posts.index', compact('posts'));
    }

    public function create(Request $request)
    {
        if ($request->isMethod('get')){
            $categories=Category::get()->pluck('name', 'id')->toArray();
            return view('posts.form',compact('categories'));
        }else {
            //echo "<pre>";print_r($request->category_id);exit;
            $rules = [
                'title' => 'required',
                'content' => 'required',
                'slug' => 'required|alpha_dash|max:200|unique:postlanguages,slug',     
            ];
            $this->validate($request, $rules);
            $post = new Post();
            $postlanguage = new Postlanguage();
            $inserArr=array();
            if($request->hasFile('image')) {
                //echo "<pre>";print_r($request->file('image'));exit;
                foreach($request->file('image') as $file)
                {
                    $dir = 'uploads/';
                    $extension = strtolower($file->getClientOriginalExtension()); // get image extension
                    $fileName = str_random() . '.' . $extension; // rename image
                    $file->move($dir, $fileName);
                    $data[] = $fileName; 
                }
                
                $postlanguage->image=implode(",",$data);
            }
           
            $post->save();

            if(!empty($post->id))
            {                
                $postlanguage->post_id  = $post->id;
                $postlanguage->title    = $request->title;
                $postlanguage->slug     = $request->slug;
                $postlanguage->summary  = $request->summary;
                $postlanguage->content  = $request->content;
                $postlanguage->lang_id  = $this->getLang->id;
                $postlanguage->save();
            }
            if(!empty($request->category_id))
            {
                $postCategoryArr=array();
                foreach($request->category_id as $post_category)
                {
                    $postCategoryArr['post_id']=$post->id;
                    $postCategoryArr['category_id']=$post_category;
                    $postCategoryArr['lang_id']=$this->getLang->id;
                    $insertpostcats = DB::table('posts_categories')->insert($postCategoryArr);
                }
                //$post->categories()->sync($request->category_id,[$post->id],[$this->getLang->id]);
            }
            return redirect('/blogposts');
        }
    }

    public function update(Request $request, $id)
    {
        //echo "fsfs";exit;
        //echo $request->isMethod('get');exit;
        if ($request->isMethod('get')){
            $categories=Category::get()->pluck('name', 'id')->toArray();
            $post=Post::find($id);
            
            $post_lang=Postlanguage::where('post_id',$id)->where('lang_id',$this->getLang->id)->first();
            //echo "<pre>";print_r($post_lang);exit;   
            $post_categories=DB::table('posts_categories')->where('post_id',$id)->where('lang_id',$this->getLang->id)->get()->toArray();
            $selected_categories = [];
            
            foreach($post_categories as $category){
                array_push($selected_categories, $category->category_id);
            }
            //echo "<pre>";print_r($selected_categories);exit;   
            return view('posts.form', compact('post','categories','selected_categories','post_lang'));
        }else {
            //echo "<pre>";print_r($_POST);exit;
            $post_id=$id;
            $rules = [
                'title' => 'required',
                'content' => 'required',
                'slug' => 'required|alpha_dash|max:200',
            ];
            $this->validate($request, $rules);
            $post = Post::find($id);
            $postID = $post->id;
            //echo "<pre>";print_r($post);exit;
            $insert_post_lang= new Postlanguage();
            $post_lang=Postlanguage::where('post_id',$id)->where('lang_id',$this->getLang->id)->first();
            $updateArr=array();
            if ($request->hasFile('image')) {
                foreach($request->file('image') as $file)
                {
                    $dir = 'uploads/';
                    $extension = strtolower($file->getClientOriginalExtension()); // get image extension
                    $fileName = str_random() . '.' . $extension; // rename image
                    $file->move($dir, $fileName);
                    $data[] = $fileName; 
                }
                if(!empty($post_lang->image))
                {
                    if(strpos($post_lang->image, ',') !== false)
                    {
                        $imgArr=explode(',',$post_lang->image);
                        $dataArr=array_merge($data,$imgArr);
                        //$post->image = implode(",",$dataArr);
                        $insert_post_lang->image = implode(",",$dataArr);
                        $updateArr['image']=implode(",",$dataArr);
                    }
                    else
                    {
                        array_push($data, $post_lang->image);
                        //$post->image = implode(",",$data);
                        $insert_post_lang->image = implode(",",$data);
                        $updateArr['image']=implode(",",$data);
                    }
                }
                else
                {
                    //$post->image = implode(",",$data);
                    $insert_post_lang->image = implode(",",$data);
                    $updateArr['image']=implode(",",$data);
                }
            }
            if(!empty($id))
            {                
                if(empty($post_lang))
                {
                    $insert_post_lang->post_id  = $postID;
                    $insert_post_lang->title    = $request->title;
                    $insert_post_lang->slug     = $request->slug;
                    $insert_post_lang->summary  = $request->summary;
                    $insert_post_lang->content  = $request->content;
                    $insert_post_lang->lang_id  = $this->getLang->id;
                    $insert_post_lang->save();
                }
                else
                {
                    $updateArr['title']   =  $request->title;
                    $updateArr['slug']    =  $request->slug;
                    $updateArr['summary'] =  $request->summary;
                    $updateArr['content'] =  $request->content;
                    $affectedRows = Postlanguage::where('post_id',$id)->where('lang_id',$this->getLang->id)->update($updateArr);
                }
            }
            if(!empty($request->category_id))
            {
                $deleterows = DB::table('posts_categories')->where('post_id',$id)->where('lang_id',$this->getLang->id)->delete();
                foreach($request->category_id as $post_category)
                {
                    $postCategoryArr['post_id']=$id;
                    $postCategoryArr['category_id']=$post_category;
                    $postCategoryArr['lang_id']=$this->getLang->id;
                    $insertpostcats = DB::table('posts_categories')->insert($postCategoryArr);
                }
                //$post->categories()->sync($request->category_id,[$id]);
            }
            return redirect('/blogposts');
        }
    }

    public function delete($id)
    {
        $post=Post::find($id);
        $post_lang=Postlanguage::where('post_id',$id)->get();
        foreach($post_lang as $post_langs)
        {
            if(File::exists('uploads/' . $post_langs->image)) {
                File::delete('uploads/' . $post_langs->post_image);
            }
        }
        DB::table('postlanguages')->where('post_id',$id)->delete();
        DB::table('posts_categories')->where('post_id',$id)->delete();
        $post->delete();
        return redirect('/blogposts');
    }

    public function ajaxrequest()
    {
        //echo "<pre>";print_r($_POST);exit;
        if(!empty($_POST))
        {
            $post_id=$_POST['post_id'];
            $image_name=$_POST['image_name'];
            $lang_id=$_POST['lang_id'];
            $PostImage=Postlanguage::where('post_id',$post_id)->where('lang_id',$lang_id)->first();
            //echo "<pre>";print_r($PostImage);exit;
            //$PostImage=Post::find($post_id);
            $updateArr=array();
            if(!empty($PostImage))
            {
                $post_image=$PostImage->image;
                if(strpos($post_image, ',') !== false)
                {
                    $explodearr = explode(',',$post_image);
                    if(in_array($image_name, $explodearr))
                    {
                        File::delete('uploads/' . $image_name);
                        if (($key = array_search($image_name, $explodearr)) !== false) {
                            unset($explodearr[$key]);
                        }
                        $updateArr['image'] =  implode(',',$explodearr);
                        $affectedRows = Postlanguage::where('post_id',$post_id)->where('lang_id',$lang_id)->update($updateArr);
                        return response()->json(['success'=>'Got Simple Ajax Request.']);
                    }
                    else
                    {
                       return response()->json(['success'=>'Error in image delete']);
                    }
                }
                else
                {
                    File::delete('uploads/' . $image_name);
                    $updateArr['image'] = null;
                    $affectedRows = Postlanguage::where('post_id',$post_id)->where('lang_id',$lang_id)->update($updateArr);
                    return response()->json(['success'=>'Got Simple Ajax Request.']);
                }
            }
            else
            {
                return response()->json(['success'=>'Error in image delete']);
            }
        }
    }
}
